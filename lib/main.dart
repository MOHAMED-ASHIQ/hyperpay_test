import 'dart:convert';
import 'dart:developer' as dev;
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:hyperpay_plugin/flutter_hyperpay.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late FlutterHyperPay flutterHyperPay;
  @override
  void initState() {
    flutterHyperPay = FlutterHyperPay(
      shopperResultUrl: InAppPaymentSetting.shopperResultUrl,
      paymentMode: PaymentMode.test,
      lang: InAppPaymentSetting.getLang(),
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Payment"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "pay with ready ui".toUpperCase(),
              style: const TextStyle(fontSize: 20, color: Colors.red),
            ),
            InkWell(
              onTap: () async {
                String? checkoutId = await getCheckOut();
                if (checkoutId != null) {
                  /// Brands Names [ VISA , MASTER , MADA , STC_PAY , APPLEPAY]
                  payRequestNowReadyUI(
                    brandsName: [
                      "VISA",
                      "MASTER",
                    ],
                    checkoutId: checkoutId,
                  );
                }
              },
              child: const Text(
                "[VISA,MASTER,MADA,STC_PAY,APPLEPAY]",
                style: TextStyle(fontSize: 20),
              ),
            ),
            const Divider(),
          ],
        ),
      ),
    );
  }

  /// URL TO GET CHECKOUT ID FOR TEST
  /// http://dev.hyperpay.com/hyperpay-demo/getcheckoutid.php
  /// Brands Names [ VISA , MASTER , MADA , STC_PAY , APPLEPAY]

  getCheckOut() async {
    // final url =
    //     Uri.parse('https://dev.hyperpay.com/hyperpay-demo/getcheckoutid.php');
    // final response = await http.get(url);
    final urlTest = Uri.parse(
        'https://v1.test.payment.araby.ai/payment/hyperpay/checkout/create');
    final responseTest = await http.post(urlTest);
    // final url2 = Uri.parse('https://eu-test.oppwa.com/v1/checkouts');
    // final response2 = await http.post(url2, body: {
    //   "amount": "92.00",
    //   "currency": "USD",
    //   "paymentType": "DB",
    //   "entityId": "8a8294174b7ecb28014b9699220015ca",
    // }, headers: {
    //   "Authorization":
    //       "Bearer OGE4Mjk0MTc0YjdlY2IyODAxNGI5Njk5MjIwMDE1Y2N8c3k2S0pzVDg="
    // });
    print(
        "asdasd: ${json.decode(responseTest.body)['id']}  &&&  ${json.decode(responseTest.body)['id']}  &&&  ${json.decode(responseTest.body)['id']}");
    if (responseTest.statusCode == 200) {
      payRequestNowReadyUI(
          checkoutId: json.decode(responseTest.body)['id'],
          brandsName: [
            "VISA",
            "MASTER",
          ]);
    } else {
      dev.log(responseTest.body.toString(), name: "STATUS CODE ERROR");
    }
  }

  payRequestNowReadyUI(
      {required List<String> brandsName, required String checkoutId}) async {
    try {
      PaymentResultData paymentResultData = await flutterHyperPay.readyUICards(
        readyUI: ReadyUI(
            brandsName: brandsName,
            checkoutId: checkoutId,
            merchantIdApplePayIOS: InAppPaymentSetting.merchantId, // applepay
            countryCodeApplePayIOS: InAppPaymentSetting.countryCode, // applePay
            companyNameApplePayIOS: "Test Co", // applePay
            themColorHexIOS: "#000000", // FOR IOS ONLY
            setStorePaymentDetailsMode:
                true // store payment details for future use
            ),
      );

      print('my paymnet status ${paymentResultData.paymentResult}');

      // if (paymentResultData.paymentResult == PaymentResult.success ||
      //     paymentResultData.paymentResult == PaymentResult.sync) {
      //   final url = Uri.parse(
      //       'https://dev.hyperpay.com/hyperpay-demo/requestCheckoutInfo.php?checkoutId=$checkoutId');
      //   final response = await http.get(url);
      //   if (response.statusCode == 200) {
      //     print("asdasd: ${response.body}");
      //   } else {
      //     dev.log("This is a log Error:");
      //     dev.log(response.body.toString(), name: "STATUS CODE ERROR");
      //   }
      // }
    } catch (e) {
      print('My money is gone $e');
    }
  }
}

class InAppPaymentSetting {
  static const String shopperResultUrl = "com.testpayment.payment";
  static const String merchantId = "MerchantId";
  static const String countryCode = "SA";
  static getLang() {
    if (Platform.isIOS) {
      return "en"; // ar
    } else {
      return "en_US"; // ar_AR
    }
  }
}
